//
//  CiFUser.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#define VALUE_U_I_D(UU, II, DD) UU.II = (DD[@"II"]==nil)?[DD[@"II"] integerValue]:0

#import "CiFUser.h"

@implementation CiFUser

static CiFUser *sharedUser;

+(CiFUser *) currentUser
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUser = [[CiFUser alloc] init];
    });
    return sharedUser;
}

+(void) setCurrentUserFBID:(CiFUser *) anUser
{
    CiFUser *_sharedUser = [CiFUser currentUser];
    _sharedUser = anUser;
}

+(CiFUser *) userFromJSON:(id)JSON
{    
    NSDictionary *d = (NSDictionary *) JSON;
    CiFUser *user = [[CiFUser alloc] init];
    
    // properties might be nil
    user.userFBID = d[@"user_fbid"];
    user.userName = d[@"user_name"];
    VALUE_U_I_D(user, lucky, d);
    VALUE_U_I_D(user, gold, d);
    VALUE_U_I_D(user, diamond, d);
    
    return user;
}

-(id) init
{
    if (self = [super init]) {
        self.userFBID = @"";
        self.userName = @"";
        self.gold = 0;
        self.diamond = 0;
        self.lucky = 0;
    }
    return self;
}

static NSMutableArray *sharedFriendsCheckins = nil;
+(NSMutableArray *) friendsCheckins
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedFriendsCheckins = [[NSMutableArray alloc] init];
    });
    return sharedFriendsCheckins;
}

@end
