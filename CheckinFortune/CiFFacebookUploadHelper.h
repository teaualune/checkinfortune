//
//  CiFFacebookUploadHelper.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@interface CiFFacebookUploadHelper : NSObject

+(void) getFriendsCheckins:(FBRequestHandler)completion;

@end
