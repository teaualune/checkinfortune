//
//  PlacePickerTableViewController.h
//  CheckinFortune
//
//  Created by Li-YUan on 12/15/12.
//  Copyright (c) 2012 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>

@interface PlacePickerTableViewController : UITableViewController<CLLocationManagerDelegate, FBPlacePickerDelegate>{
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocationCoordinate2D locationCoordinate;
@property (nonatomic) NSArray *placeResult;



@end
