//
//  CiFPostResultViewController.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/15.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFPostResultViewController.h"

#import "CiFMenuViewController.h"
#import "CiFCoreCheckinRecord.h"
#import "CiFAnnotation.h"
#import "CiFUser.h"

@interface CiFPostResultViewController ()
{
    BOOL hasPhotoToUpload;
}

@property (strong, nonatomic) FBFriendPickerViewController *friendPickerViewController;

@end

@implementation CiFPostResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    CLLocationCoordinate2D currentLocation = [CiFCoreCheckinRecord currentCheckin].coordinate;
    
    tags = [[NSMutableSet alloc] init];
    backgroundMapView.centerCoordinate = currentLocation;
    backgroundMapView.region = MKCoordinateRegionMake(currentLocation, MKCoordinateSpanMake(0.001, 0.001));
    CiFAnnotation *annotation = [[CiFAnnotation alloc] initWithCoordinate:backgroundMapView.centerCoordinate Title:@"" AndSubtitle:@""];
    [backgroundMapView addAnnotation:annotation];
    
    backgroundMapView.userInteractionEnabled = NO;
    
    CiFUser *user = [CiFUser currentUser];
    
    FBProfilePictureView *profilePictureView = [[FBProfilePictureView alloc] initWithProfileID:user.userFBID pictureCropping:FBProfilePictureCroppingSquare];
    profilePictureView.frame = CGRectMake(20, 20, 52, 52);
    [containerView addSubview:profilePictureView];
    
    self.luckyLabel.text = [NSString stringWithFormat:@"%d", [CiFCoreCheckinRecord currentCheckin].allLucky];
    
    hasPhotoToUpload = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.messageTextView becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    backgroundMapView.delegate = nil;
}

-(IBAction)post:(id)sender
{
    __block CiFCheckin *checkin = [CiFCoreCheckinRecord currentCheckin];
    checkin.message = [[NSString stringWithFormat:@"得到 %d 點幸運值！\n", checkin.allLucky] stringByAppendingString:self.messageTextView.text];
    
    checkin.userFBID = [CiFUser currentUser].userFBID;
    
    [FBSession openActiveSessionWithPublishPermissions:@[@"publish_checkins", @"publish_stream"] defaultAudience:FBSessionDefaultAudienceOnlyMe allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        
        // NSLog(@"%@\n%@\n%@", [tags allObjects], checkin.message, checkin.placeFBID);
        
        FBRequest *request;
        
        if (hasPhotoToUpload) { // has picture
            request = [FBRequest requestForUploadPhoto:uploadPhotoView.image];
            
            request.parameters[@"message"] = checkin.message;
            request.parameters[@"place"] = checkin.placeFBID;
            request.parameters[@"tags"] = [tags allObjects];
        } else {
            request = [FBRequest requestForPostStatusUpdate:checkin.message
                                              place:checkin.placeFBID
                                               tags:[tags allObjects]];
        }
        
        [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (error) NSLog(@"%@", error);
            NSLog(@"Complete!\n%@", result);
            
            
            
            checkin.CheckinFBID = [NSString stringWithFormat:@"%@", result[@"id"]];
            NSMutableDictionary *muDic = [[NSMutableDictionary alloc]init];
            [muDic setObject:checkin.CheckinFBID forKey:@"CheckinFBID"];
            [muDic setObject:checkin.userFBID forKey:@"userFBID"];
            [muDic setObject:checkin.placeFBID forKey:@"placeFBID"];
            [muDic setObject:checkin.message forKey:@"message"];
            NSString *strLatitude = [NSString stringWithFormat:@"%f",checkin.coordinate.latitude];
            [muDic setObject:strLatitude forKey:@"latitude"];
            NSString *strLongitude = [NSString stringWithFormat:@"%f",checkin.coordinate.longitude];
            [muDic setObject:strLongitude forKey:@"longitude"];
            [muDic setObject:[NSNumber numberWithInt:checkin.offLucky] forKey:@"offLucky"];
            [muDic setObject:[NSNumber numberWithInt:checkin.onLucky] forKey:@"onLucky"];
            [muDic setObject:[NSNumber numberWithInt:checkin.allLucky] forKey:@"allLucky"];
            [muDic setObject:[NSNumber numberWithInt:checkin.gold] forKey:@"gold"];
            [muDic setObject:[NSNumber numberWithInt:checkin.diamond] forKey:@"diamond"];
            NSString *fileName = [self documentPath:@"selfCheck.plist"];
            NSMutableArray *arrayDic;
            if ([[NSFileManager defaultManager] fileExistsAtPath:fileName])
            {
                arrayDic = [[NSMutableArray alloc]initWithContentsOfFile:fileName];
            }
            else
            {
                arrayDic = [[NSMutableArray alloc]init];
            }
            [arrayDic addObject:muDic];
            NSLog(@"arrayDic:%@",arrayDic);
            [arrayDic writeToFile:[self documentPath:@"selfCheck.plist"] atomically:YES];
            
            
            [UIView animateWithDuration:0.06 animations:^{
                
                containerView.transform = CGAffineTransformMakeScale(1.3, 1.3);
                
            } completion:^(BOOL finished) {
                
                [UIView animateWithDuration:0.6 animations:^{
                    
                    containerView.transform = CGAffineTransformMakeScale(0.0, 0.0);
                    containerView.center = [[UIApplication sharedApplication].delegate window].center;
                    
                } completion:^(BOOL finished) {
                    
                    [self dismissViewControllerAnimated:YES completion:^{
                        // jump to map...
                        [[CiFMenuViewController sharedMenu] showAnotherContentViewControllerAtIndex:2 animated:NO];
                    }];
                    
                }];
                
            }];
            
        }];
        
    }];
}

-(IBAction)tag:(id)sender
{
    if (self.friendPickerViewController == nil) {
        self.friendPickerViewController = [[FBFriendPickerViewController alloc] init];
        self.friendPickerViewController.delegate = self;
        self.friendPickerViewController.title = @"Tag Friends";
        self.friendPickerViewController.sortOrdering = FBFriendSortByLastName;
    }
    
    [self.friendPickerViewController clearSelection];
    [self.friendPickerViewController loadData];
    [self presentViewController:self.friendPickerViewController animated:YES completion:^{
        
    }];
}


-(IBAction)photo:(id)sender{
    //NSMutableArray *temp = [NSMutableArray arrayWithCapacity:3];
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Take a photo..."
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles: nil]
    ;
    
    int i = 0;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        [actionSheet addButtonWithTitle:@"...from photo library"];
        i++;
    }
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        [actionSheet addButtonWithTitle:@"...with camera"];
        i++;
    }
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]){
        [actionSheet addButtonWithTitle:@"...from photo album"];
        i++;
    }
    
    [actionSheet addButtonWithTitle:@"Cancel"];
    
    actionSheet.cancelButtonIndex = i;
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    pickerController = [[UIImagePickerController alloc] init];
    if([title isEqualToString:@"...from photo library"]) {
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    else if([title isEqualToString:@"...with camera"]) {
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        pickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        pickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        pickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        
    }
    
    else if([title isEqualToString:@"...from photo album"]) {
        pickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    } else {
        return;
    }
    
    pickerController.delegate = self;
    
    [self presentViewController:pickerController animated:YES completion:^(){
        //NSLog(@"back?");
    }];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    NSLog(@"cancel?");
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}


-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSLog(@"%@", info);
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        uploadPhotoView.image = image;
        hasPhotoToUpload = YES;
    }];
}

#pragma mark - FB friend picker delegate

- (void)facebookViewControllerDoneWasPressed:(id)sender {
    
    for (id<FBGraphUser> user in self.friendPickerViewController.selection) {
        [tags addObject: [NSString stringWithString: user.id]];
    }

    [self facebookViewControllerCancelWasPressed:sender];
}

- (void)facebookViewControllerCancelWasPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (NSString *)documentPath:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *document =[[NSString alloc] initWithString:[paths objectAtIndex:0]];
    return [document stringByAppendingPathComponent:filename];
}

@end
