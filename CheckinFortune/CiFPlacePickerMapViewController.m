//
//  CiFPlacePickerMapViewController.m
//  CheckinFortune
//
//  Created by Li-YUan on 12/16/12.
//  Copyright (c) 2012 Teaualune Tseng. All rights reserved.
//

#import "CiFPlacePickerMapViewController.h"
#import "CiFCoreCheckinRecord.h"


#define RESULTSLIMIT 30
#define RADIUSINMETERS 500

@interface CiFPlacePickerMapViewController (){
    NSInteger previousIndexPathRow;
    NSInteger currentIndexPathRow;
    
}

-(void) startUpdateLocation;

@end

@implementation CiFPlacePickerMapViewController

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (self.placeResult == nil) return 0;
    else return [self.placeResult count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PlacePickerCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    if (indexPath.row == currentIndexPathRow) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (self.placeResult[indexPath.row] != nil) {
        cell.textLabel.text = self.placeResult[indexPath.row][@"name"];
    }
    
    return cell;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.activityIndicator startAnimating];
    
    self.placeResult = nil;
    
    previousIndexPathRow = currentIndexPathRow = -1;
    placeMapView.delegate = self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(startUpdateLocation) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    self.chosenAnnotation = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startUpdateLocation];
    self.nextButton.enabled = NO;
}

-(void) dealloc
{
    placeMapView.delegate = nil;
    self.locationManager = nil;
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", locations);
    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;
    
    self.locationCoordinate = ((CLLocation *) locations[0]).coordinate;
    
    //start to locate your current location on the map
    
    MKCoordinateSpan currentSpan= MKCoordinateSpanMake(0.0005, 0.0005);
    placeMapView.centerCoordinate = self.locationCoordinate;
    placeMapView.region = MKCoordinateRegionMake(self.locationCoordinate, currentSpan);
    placeMapView.delegate = self;
    
    self.chosenAnnotation.coordinate = self.locationCoordinate;
    
    //end of locating the current location
    
    [FBRequestConnection startForPlacesSearchAtCoordinate:self.locationCoordinate radiusInMeters:RADIUSINMETERS resultsLimit:RESULTSLIMIT searchText:nil completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        for(NSDictionary *dic in result[@"data"]){
            NSLog(@"%@", dic);
        }
        self.placeResult = [NSArray arrayWithArray:result[@"data"]];
        
        [self.tableView reloadData];
        
        [self.activityIndicator stopAnimating];
        
        if (refreshControl.refreshing) [refreshControl endRefreshing];
    }];

}

/*
- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    if (newLocation.horizontalAccuracy < 100) {
        // We wait for a precision of 100m and turn the GPS off
        [self.locationManager stopUpdatingLocation];
        self.locationManager = nil;
        
        self.locationCoordinate = newLocation.coordinate;
        
        //start to locate your current location on the map
        
        MKCoordinateSpan currentSpan= MKCoordinateSpanMake(0.0005, 0.0005);
        placeMapView.centerCoordinate = self.locationCoordinate;
        placeMapView.region = MKCoordinateRegionMake(self.locationCoordinate, currentSpan);
        placeMapView.delegate = self;
        
        self.chosenAnnotation.coordinate = self.locationCoordinate;
        
        //end of locating the current location
        
        [FBRequestConnection startForPlacesSearchAtCoordinate:self.locationCoordinate radiusInMeters:RADIUSINMETERS resultsLimit:RESULTSLIMIT searchText:nil completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            for(NSDictionary *dic in result[@"data"]){
                NSLog(@"%@", dic);
            }
            self.placeResult = [NSArray arrayWithArray:result[@"data"]];
        
            [self.tableView reloadData];

            [self.activityIndicator stopAnimating];
            
            if (refreshControl.refreshing) [refreshControl endRefreshing];
        }];
    }
}
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    CLLocationCoordinate2D chosenLocation = CLLocationCoordinate2DMake([self.placeResult[indexPath.row][@"location"][@"latitude"] doubleValue], [self.placeResult[indexPath.row][@"location"][@"longitude"] doubleValue]);
    
    previousIndexPathRow = currentIndexPathRow;
    currentIndexPathRow = indexPath.row;
    
    if (previousIndexPathRow == currentIndexPathRow) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    if (previousIndexPathRow != -1) {
        NSIndexPath *previousIndexPath = [NSIndexPath indexPathForRow:previousIndexPathRow inSection:0];
        [tableView cellForRowAtIndexPath:previousIndexPath].accessoryType = UITableViewCellAccessoryNone;
        
        if (self.chosenAnnotation != nil) [placeMapView removeAnnotation:self.chosenAnnotation];
    }
    
    self.chosenAnnotation = [[CiFAnnotation alloc] initWithCoordinate:chosenLocation Title:self.placeResult[indexPath.row][@"name"] AndSubtitle:self.placeResult[indexPath.row][@"category"]];
    [placeMapView addAnnotation:self.chosenAnnotation];
    
    MKCoordinateSpan currentSpan= MKCoordinateSpanMake(0.001, 0.001);
    placeMapView.centerCoordinate = chosenLocation;
    placeMapView.region = MKCoordinateRegionMake(chosenLocation, currentSpan);

    [placeMapView selectAnnotation:self.chosenAnnotation animated:YES];
    
    [CiFCoreCheckinRecord currentCheckin].placeFBID = self.placeResult[indexPath.row][@"id"];
    [CiFCoreCheckinRecord currentCheckin].coordinate = CLLocationCoordinate2DMake([self.placeResult[indexPath.row][@"location"][@"latitude"] doubleValue], [self.placeResult[indexPath.row][@"location"][@"longitude"] doubleValue]);
    
    self.nextButton.enabled = YES;
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
  
    MKPinAnnotationView *marker = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"default"];
    if(!marker){
        marker = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"default"];
        marker.animatesDrop = NO;
        marker.pinColor = MKPinAnnotationColorRed;
        marker.canShowCallout = YES;
        marker.annotation = annotation;
        //marker.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
    }
   
    return marker;
}

//- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
//    [self performSegueWithIdentifier:@"MapToDetail" sender:view];
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) startUpdateLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
}

- (IBAction)cancel:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
@end
