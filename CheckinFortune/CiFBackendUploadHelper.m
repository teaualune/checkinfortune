//
//  CiFBackendUploadHelper.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFBackendUploadHelper.h"

#import "AFNetworking.h"

static NSString *backendSiteURL = @"https://??";

@interface CiFBackendUploadHelper ()

+(NSURLRequest *) requestWithPath:(NSString *) path method:(NSString *)method body:(NSDictionary *)body;

+(void) runNullResponseRequest:(NSURLRequest *) request completion: (NullResponseHandler) completeHandler;

+(void) runCheckinsResponseRequest:(NSURLRequest *) request completion:(CheckinsResponseHandler) completeHandler;

+(void) runCheckinIDCoordinateResponseRequest:(NSURLRequest *) request completion:(CheckinIDCoordinateResponseHandler) completeHandler;

@end

@implementation CiFBackendUploadHelper

#pragma mark - private methods

+(NSURLRequest *) requestWithPath:(NSString *) path method:(NSString *)method body:(NSDictionary *)body
{
    NSMutableDictionary *md = [NSMutableDictionary dictionaryWithDictionary:body];
    [md setValue:path forKey:@"fun"];
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:backendSiteURL]];
    [request setHTTPMethod:method];
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:md options:0 error:nil]];
    return request;
}

+(void) runNullResponseRequest:(NSURLRequest *) request completion:(NullResponseHandler) completeHandler
{
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        completeHandler(nil);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        completeHandler(error);
    }];
    [requestOperation start];
}

+(void) runCheckinsResponseRequest:(NSURLRequest *) request completion:(CheckinsResponseHandler) completeHandler
{
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        completeHandler([CiFCheckin checkinsFromJSON:JSON], nil);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        completeHandler([CiFCheckin checkinsFromJSON:JSON] , error);
    }];
    [requestOperation start];
}

+(void) runCheckinIDCoordinateResponseRequest:(NSURLRequest *)request completion:(CheckinIDCoordinateResponseHandler)completeHandler
{
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        completeHandler([CiFCheckin checkinIDCoordinatesFromJSON:JSON], nil);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        completeHandler([CiFCheckin checkinIDCoordinatesFromJSON:JSON] , error);
    }];
    [requestOperation start];
}

#pragma mark - public methods

+(void) addUserComplete: (NullResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{@"user_fbid" : [CiFUser currentUser].userFBID, @"user_name" : @"Lawrence"};
    
    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"addUser" method:@"POST" body:body];
    
    [CiFBackendUploadHelper runNullResponseRequest:request completion:completeHandler];
}

+(void) updateFriends:(NSArray *) userFriends complete: (NullResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{@"user_friends_fbid" : @[@"234", @"345", @"456", @"567"]};
    
    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"updateFriends" method:@"POST" body:body];
    
    [CiFBackendUploadHelper runNullResponseRequest:request completion:completeHandler];
}

+(void) addCheckin:(CiFCheckin *) checkin complete: (NullResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{
        @"user_fbid" : [CiFUser currentUser].userFBID,
        @"place_fbid" : checkin.placeFBID,
        @"time" : checkin.time,
        @"offLucky" : [NSNumber numberWithInteger: checkin.offLucky],
        @"onLucky" : [NSNumber numberWithInteger: checkin.onLucky],
        @"allLucky" : [NSNumber numberWithInteger: checkin.allLucky],
        @"gold" : [NSNumber numberWithInteger: checkin.gold],
        @"diamond" : [NSNumber numberWithInteger: checkin.diamond],
    };

    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"addCheckin" method:@"POST" body:body];
    
    [CiFBackendUploadHelper runNullResponseRequest:request completion:completeHandler];
}

+(void) getUserByFBID:(NSString *) fbid complete: (UserResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{@"user_fbid" : [CiFUser currentUser].userFBID};

    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"getUser" method:@"GET" body:body];
    
    // do not refactor here
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        completeHandler([CiFUser userFromJSON:JSON], nil);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        completeHandler([CiFUser userFromJSON:JSON], error);
    }];
    [requestOperation start];
}

+(void) getCheckinsByIDs:(NSArray *) checkinIDs complete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{
    @"checkin_ids": checkinIDs
    };
    
    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"getCheckinsByIDs" method:@"GET" body:body];
    
    [CiFBackendUploadHelper runCheckinsResponseRequest:request completion:completeHandler];
}

+(void) getCheckinsByFBIDs:(NSArray *) checkinFBIDs complete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body =@{
    @"checkin_fbid" : checkinFBIDs
    };

    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"getCheckinsByFBIDs" method:@"GET" body:body];
    
    [CiFBackendUploadHelper runCheckinsResponseRequest:request completion:completeHandler];
}

+(void) getUserCheckinsComplete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{@"user_fbid" : [CiFUser currentUser].userFBID};
    
    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"getUserCheckins" method:@"GET" body:body];
    
    [CiFBackendUploadHelper runCheckinsResponseRequest:request completion:completeHandler];
}

+(void) getFriendCheckinsPaging:(NSInteger) pagingIndex complete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{@"user_fbid" : [CiFUser currentUser].userFBID, @"page" : [NSNumber numberWithInteger:pagingIndex]};
    
    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"getFriendCheckins" method:@"GET" body:body];
    
    [CiFBackendUploadHelper runCheckinsResponseRequest:request completion:completeHandler];
}

+(void) getUserCheckinsCoordinateComplete: (CheckinIDCoordinateResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{@"user_fbid" : [CiFUser currentUser].userFBID};
    
    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"getUserCheckinsCoordinate" method:@"GET" body:body];
    
    [CiFBackendUploadHelper runCheckinIDCoordinateResponseRequest:request completion:completeHandler];
}

+(void) getFriendCheckinsCoordinatePaging:(NSInteger) pagingIndex complete: (CheckinIDCoordinateResponseHandler) completeHandler isAsynchronous:(BOOL) isa
{
#pragma warning - pseudo data
    NSDictionary *body = @{@"user_fbid" : [CiFUser currentUser].userFBID, @"page" : [NSNumber numberWithInteger:pagingIndex]};
    
    NSURLRequest *request = [CiFBackendUploadHelper requestWithPath:@"getFriendCheckinsCoordinate" method:@"GET" body:body];
    
    [CiFBackendUploadHelper runCheckinIDCoordinateResponseRequest:request completion:completeHandler];
}

@end
