//
//  CiFCoreCheckinRecord.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/15.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFCoreCheckinRecord.h"

@implementation CiFCoreCheckinRecord

static CiFCheckin *checkin = nil;
+(CiFCheckin *) currentCheckin
{
    if (checkin == nil) {
        checkin = [[CiFCheckin alloc] init];
    }
    return checkin;
}

@end
