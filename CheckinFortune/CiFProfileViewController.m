//
//  CiFProfileViewController.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/19.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFProfileViewController.h"

#import "CiFMenuViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CiFUser.h"

@interface CiFProfileViewController ()

@end

@implementation CiFProfileViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CiFUser *user = [CiFUser currentUser];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.goldLabel.text = [self.goldLabel.text stringByAppendingFormat:@"%d", user.gold];
    self.diamondLabel.text = [self.diamondLabel.text stringByAppendingFormat:@"%d", user.diamond];
    self.luckyLabel.text = [self.luckyLabel.text stringByAppendingFormat:@"%d", user.lucky];
    
    CGRect frame = self.profileView.frame;
    UIView *superViewOfProfile = self.profileView.superview;
    [self.profileView removeFromSuperview];
    
    FBProfilePictureView *ppv = [[FBProfilePictureView alloc] initWithProfileID:user.userFBID pictureCropping:FBProfilePictureCroppingSquare];
    ppv.frame = frame;
    [superViewOfProfile addSubview:ppv];
    self.nameLabel.text = [CiFUser currentUser].userName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 5;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    FBProfilePictureView *profileView;
    CiFUser *user = [CiFUser currentUser];
    switch (indexPath.row) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:@"pcProfile" forIndexPath:indexPath];
            profileView = [[FBProfilePictureView alloc] initWithProfileID:user.userFBID pictureCropping:FBProfilePictureCroppingSquare];
            profileView.frame = CGRectMake(20, 20, 120, 120);
            [cell addSubview:profileView];
            break;
        case 1:
            cell = [tableView dequeueReusableCellWithIdentifier:@"pcGold" forIndexPath:indexPath];
            cell.textLabel.text = [NSString stringWithFormat:@"金幣 : %d", user.gold];
            break;
        case 2:
            cell = [tableView dequeueReusableCellWithIdentifier:@"pcDiamond" forIndexPath:indexPath];
            cell.textLabel.text = [NSString stringWithFormat:@"鑽石 : %d", user.diamond];
            break;
        case 3:
            cell = [tableView dequeueReusableCellWithIdentifier:@"pcLucky" forIndexPath:indexPath];
            cell.textLabel.text = [NSString stringWithFormat:@"幸運值 : %d", user.lucky];
            break;
        case 4:
            cell = [tableView dequeueReusableCellWithIdentifier:@"pcSetting" forIndexPath:indexPath];
            cell.textLabel.text = @"設定";
            break;
        default:
            break;
    }
    
    // Configure the cell...
    
    return cell;
}
 */

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(IBAction)showMenu:(id)sender
{
    [[CiFMenuViewController sharedMenu] showMenu:sender];
}

@end
