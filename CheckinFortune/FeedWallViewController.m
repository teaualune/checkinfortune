//
//  FeedWallViewController.m
//  CheckinFortune
//
//  Created by En on 12/12/17.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "FeedWallViewController.h"
#import "CiFBackendUploadHelper.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CiFMenuViewController.h"
#import "CiFUser.h"

#define TABLEME 0
#define TABLEFRIEND 1

@interface FeedWallViewController ()

-(void) friendsCheckinsUpdated:(id)sender;

@end

@implementation FeedWallViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //array init
    arrayOfMyCheck = [[NSMutableArray alloc]init];
    arrayOfFriendCheck = [[NSMutableArray alloc]init];
    
    //set Type
    tableType = TABLEME;
    
    NSString *fileName = [self documentPath:@"selfCheck.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileName])
    {
        arrayInPlist = [[NSMutableArray alloc]initWithContentsOfFile:fileName];
        for (NSMutableDictionary *dic in arrayInPlist)
        {
            [FBRequestConnection startWithGraphPath:dic[@"placeFBID"] completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
            {
                NSLog(@"result:%@",result);
                [self reloadTheData:result];
            }];
            
        }
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(friendsCheckinsUpdated:) name:@"CiFGetFriendsChekinsNotification" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView delegate Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableType == TABLEME)
    {
        return [arrayOfMyCheck count];
    }
    else
    {
        return [[CiFUser friendsCheckins] count];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        
    }
    if (tableType == TABLEME)
    {
        cell.textLabel.text= [(NSDictionary *)[arrayOfMyCheck objectAtIndex:indexPath.row]objectForKey:@"name"];
    }
    else
    {
        cell.textLabel.text= [(NSDictionary *)[[CiFUser friendsCheckins] objectAtIndex:indexPath.row] objectForKey:@"place"][@"name"];

    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableType == TABLEME)
    {
        for (NSMutableDictionary *dic in arrayInPlist)
        {
            NSLog(@"id:%@",[dic objectForKey:@"placeID"]);
            if ([[dic objectForKey:@"placeFBID"] isEqualToString:[(NSDictionary *)[arrayOfMyCheck objectAtIndex:indexPath.row]objectForKey:@"id"]])
            {
                //NSValue *value = [FeedWallViewController valueWithData:[dic objectForKey:@"coordinate"]];
                
                MKCoordinateSpan currentSpan= MKCoordinateSpanMake(0.001, 0.001);
                CLLocationCoordinate2D old_coordinate;
                old_coordinate.latitude = [[dic objectForKey:@"latitude"]doubleValue];
                old_coordinate.longitude = [[dic objectForKey:@"longitude"]doubleValue];
                mapView.centerCoordinate = old_coordinate;
                mapView.region = MKCoordinateRegionMake(old_coordinate, currentSpan);
            }
        }
    }
    else
    {
    
    }
    
}

-(IBAction)changeType:(id)sender
{
    UIButton *btnType = (UIButton *)sender;
    if (btnType.tag == TABLEME)
    {
        tableType = TABLEME;
    }
    else if (btnType.tag == TABLEFRIEND)
    {
        tableType = TABLEFRIEND;
    }
    [tableCheck reloadData];
}

- (NSString *)documentPath:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *document =[[NSString alloc] initWithString:[paths objectAtIndex:0]];
    return [document stringByAppendingPathComponent:filename];
}

-(void)reloadTheData:(id)reslut
{
    [arrayOfMyCheck addObject:reslut];
    
    [tableCheck reloadData];
}

-(IBAction)show:(id)sender
{
    [[CiFMenuViewController sharedMenu] showMenu:sender];
}

-(void) friendsCheckinsUpdated:(id)sender
{
    if (tableType == TABLEFRIEND)
    [tableCheck reloadData];
}

@end
