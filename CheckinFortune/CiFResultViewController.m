//
//  CiFResultViewController.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/15.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFResultViewController.h"
#import "CiFCoreCheckinRecord.h"
#import <QuartzCore/QuartzCore.h>

@interface CiFResultViewController ()

@property (nonatomic) NSInteger gold;
@property (nonatomic) NSInteger diamond;
@property (nonatomic) NSInteger onLucky;
@property (nonatomic) NSInteger offLucky;
@property (nonatomic) NSInteger allLucky;

@end

@implementation CiFResultViewController

NSInteger leftStatus=0;
NSInteger midStatus=0;
NSInteger rightStatus=0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    CiFCheckin *checkin = [CiFCoreCheckinRecord currentCheckin];
    
    self.gold = checkin.gold;
    self.diamond = checkin.diamond;
    self.onLucky = checkin.onLucky;
    self.offLucky = checkin.offLucky;
    self.allLucky = checkin.allLucky;
    
    self.goldLabel.text = [NSString stringWithFormat:@"%d", self.gold];
    self.diamondLabel.text = [NSString stringWithFormat:@"%d", self.diamond];
    self.onLuckyLabel.text = [NSString stringWithFormat:@"%d", self.onLucky];
    self.offLuckyLabel.text = [NSString stringWithFormat:@"%d", self.offLucky];
    self.allLuckyLabel.text = [NSString stringWithFormat:@"%d", self.allLucky];
    
    if (self.allLucky > 200) {
        self.luckyLabel.text = @"大吉";
    } else if (self.allLucky > 100) {
        self.luckyLabel.text = @"吉";
    } else {
        self.luckyLabel.text = @"小吉";
    }
    
    [self.leftRsltAnimal addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressLeft:)]]; // if tap leftRsltAnimal, run [self pressLeft:...];
    [self.rightRsltAnimal addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressRight:)]]; // if tap leftRsltAnimal, run [self pressRight:...];
    [self.midRsltAnimal addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressMid:)]]; // if tap leftRsltAnimal, run [self pressMid:...];
    
    self.leftRsltAnimal.userInteractionEnabled = YES;
    self.rightRsltAnimal.userInteractionEnabled = YES;
    self.midRsltAnimal.userInteractionEnabled = YES;
    
    for ( UIImageView *elem in [self leftRsltCloud]) {
        elem.alpha=0;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self allSlideUp];
}

-(IBAction)goToPostView:(id)sender
{
    UIView *blackView = [[UIView alloc] initWithFrame:[[UIApplication sharedApplication].delegate window].frame];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.0;
    [[[UIApplication sharedApplication].delegate window] addSubview:blackView];
    
    [UIView transitionWithView:blackView
                      duration:0.2
                       options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionTransitionNone
                    animations:^{
                        blackView.alpha = 1.0;
                    }
                    completion:^(BOOL finished) {
                        
                        [self.navigationController dismissViewControllerAnimated:NO completion:^{
                            [[[UIApplication sharedApplication].delegate window].rootViewController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CiFPostResultViewController"] animated:NO completion:^{
                                
                                [UIView transitionWithView:blackView
                                                  duration:0.2
                                                   options:UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionTransitionNone
                                                animations:^{
                                                    blackView.alpha = 0.0;
                                                }
                                                completion:^(BOOL finished) {
                                                    [blackView removeFromSuperview];
                                }];
                                
                            }];
                        }];
                        
                        
    }];
//    
//    [UIView animateWithDuration:1.0 animations:^{
//        blackView.layer.backgroundColor = [UIColor blackColor].CGColor;
//    } completion:^(BOOL finished) {
//        [blackView removeFromSuperview];
//        [self.navigationController dismissViewControllerAnimated:NO completion:^{
//            [[[UIApplication sharedApplication].delegate window].rootViewController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CiFPostResultViewController"] animated:NO completion:^{
//                
//            }];
//        }];
//        
//    }];
}


-(void) allSlideUp
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.leftRsltAnimal.center= CGPointMake(self.leftRsltAnimal.center.x, self.leftRsltAnimal.center.y - 20);
                         self.midRsltAnimal.center= CGPointMake(self.midRsltAnimal.center.x, self.midRsltAnimal.center.y - 20);
                         self.rightRsltAnimal.center= CGPointMake(self.rightRsltAnimal.center.x, self.rightRsltAnimal.center.y - 20);
                         //                         self.leftRsltCloud.alpha=1;
                         for ( UIImageView *elem in [self leftRsltCloud]) {
                             elem.alpha=1;
                         }
                         self.midRsltCloud.alpha=1;
                         self.rightRsltCloud.alpha=1;
                     }
                     completion:^(BOOL finished) {
                         //[self allSlideDown];
                     }];
}

#pragma mark - test show num

-(void) allSlideDown
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.leftRsltAnimal.center= CGPointMake(self.leftRsltAnimal.center.x, self.leftRsltAnimal.center.y + 60);
                         self.midRsltAnimal.center= CGPointMake(self.midRsltAnimal.center.x, self.midRsltAnimal.center.y + 60);
                         self.rightRsltAnimal.center= CGPointMake(self.rightRsltAnimal.center.x, self.rightRsltAnimal.center.y + 60);
                         //                         self.leftRsltCloud.alpha=0;
                         for ( UIImageView *elem in [self leftRsltCloud]) {
                             elem.alpha=0;
                         }
                         self.midRsltCloud.alpha=0;
                         self.rightRsltCloud.alpha=0;
                     }
                     completion:^(BOOL finished) {
                         [self showNumDecimalPos:self.decimalNum singlePos:self.singleNum num:self.allLucky];
                     }];
}

-(void) slideUpLeft
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.leftRsltAnimal.center= CGPointMake(self.leftRsltAnimal.center.x, self.leftRsltAnimal.center.y - 40);
                         //                         self.leftRsltCloud.alpha=1;
                         for ( UIImageView *elem in [self leftRsltCloud]) {
                             elem.alpha=1;
                         }
                         //                         self.decimalNum.image = [UIImage imageNamed:@"diamond90.png"];
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void) slideDownLeft
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.leftRsltAnimal.center= CGPointMake(self.leftRsltAnimal.center.x, self.leftRsltAnimal.center.y + 40);
                         //                         self.leftRsltCloud.alpha=0;
                         for ( UIImageView *elem in [self leftRsltCloud]) {
                             elem.alpha=0;
                         }
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

-(void) slideUpMid
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.midRsltAnimal.center= CGPointMake(self.midRsltAnimal.center.x, self.midRsltAnimal.center.y - 40);
                         self.midRsltCloud.alpha=1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void) slideDownMid
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.midRsltAnimal.center= CGPointMake(self.midRsltAnimal.center.x, self.midRsltAnimal.center.y + 40);
                         self.midRsltCloud.alpha=0;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

-(void) slideUpRight
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.rightRsltAnimal.center= CGPointMake(self.rightRsltAnimal.center.x, self.rightRsltAnimal.center.y - 40);
                         self.rightRsltCloud.alpha=1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void) slideDownRight
{
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.rightRsltAnimal.center= CGPointMake(self.rightRsltAnimal.center.x, self.rightRsltAnimal.center.y + 40);
                         self.rightRsltCloud.alpha=0;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}


-(void) pressLeft:(id)sender
{
    if(leftStatus==0){
        [self slideUpLeft];
        leftStatus=1;
        return;
    }else if(leftStatus==1){
        [self slideDownLeft];
        leftStatus=0;
        return;
    }
}

-(void) pressMid:(id)sender
{
    if(midStatus==0){
        [self slideUpMid];
        NSLog(@"midmid");
        midStatus=1;
        return;
    }else if(midStatus==1){
        [self slideDownMid];
        NSLog(@"midmid");
        midStatus=0;
        return;
    }
}

-(void) pressRight:(id)sender
{
    if(rightStatus==0){
        [self slideUpRight];
        rightStatus=1;
        return;
    }else if(rightStatus==1){
        [self slideDownRight];
        rightStatus=0;
        return;
    }
}

-(void) showNumDecimalPos:(UIImageView*)decimalPos singlePos:(UIImageView*)singlePos num:(NSInteger)rsltNum
{
    NSInteger decimalVal=rsltNum/10;
    NSInteger singleVal=rsltNum%10;
    //    NSLog(@"d=%d,s=%d",decimalVal,singalVal);
    
    NSString *decimalImg;
    NSString *singleImg;
    
    switch (decimalVal) {
        case 0:
            decimalImg=@"0.png";
            break;
        case 1:
            decimalImg=@"1.png";
            break;
        case 2:
            decimalImg=@"2.png";
            break;
        case 3:
            decimalImg=@"3.png";
            break;
        case 4:
            decimalImg=@"4.png";
            break;
        case 5:
            decimalImg=@"5.png";
            break;
        case 6:
            decimalImg=@"6.png";
            break;
        case 7:
            decimalImg=@"7.png";
            break;
        case 8:
            decimalImg=@"8.png";
            break;
        case 9:
            decimalImg=@"9.png";
            break;
        default:
            break;
    }
    switch (singleVal) {
        case 0:
            singleImg=@"0.png";
            break;
        case 1:
            singleImg=@"1.png";
            break;
        case 2:
            singleImg=@"2.png";
            break;
        case 3:
            singleImg=@"3.png";
            break;
        case 4:
            singleImg=@"4.png";
            break;
        case 5:
            singleImg=@"5.png";
            break;
        case 6:
            singleImg=@"6.png";
            break;
        case 7:
            singleImg=@"7.png";
            break;
        case 8:
            singleImg=@"8.png";
            break;
        case 9:
            singleImg=@"9.png";
            break;
        default:
            break;
    }
    
    decimalPos.image = [UIImage imageNamed:decimalImg];
    singlePos.image = [UIImage imageNamed:singleImg];
}


@end
