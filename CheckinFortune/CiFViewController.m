//
//  CiFViewController.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/22.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFViewController.h"
#import "CiFMenuViewController.h"

@interface CiFViewController ()

@end

@implementation CiFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)show:(id)sender
{
    [[CiFMenuViewController sharedMenu] showMenu:sender];
}

@end
