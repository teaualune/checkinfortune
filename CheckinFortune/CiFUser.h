//
//  CiFUser.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CiFUser : NSObject

@property (nonatomic, copy) NSString *userFBID;
@property (nonatomic, copy) NSString *userName;

@property (nonatomic) NSInteger lucky;
@property (nonatomic) NSInteger gold;
@property (nonatomic) NSInteger diamond;

+(CiFUser *) currentUser;
+(void) setCurrentUserFBID:(CiFUser *) anUser;
+(CiFUser *) userFromJSON:(id)JSON;

+(NSMutableArray *) friendsCheckins;

@end
