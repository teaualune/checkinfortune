//
//  CiFCheckin.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CiFCheckin : NSObject

@property (nonatomic, copy) NSString *checkinID;
@property (nonatomic, copy) NSString *CheckinFBID;
@property (nonatomic, copy) NSString *userFBID;
@property (nonatomic, copy) NSString *placeFBID;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSDate *time;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) NSInteger offLucky;
@property (nonatomic) NSInteger onLucky;
@property (nonatomic) NSInteger allLucky;
@property (nonatomic) NSInteger gold;
@property (nonatomic) NSInteger diamond;

+(NSArray *) checkinsFromJSON:(id)JSON;
+(NSArray *) checkinIDCoordinatesFromJSON:(id)JSON;

-(CGFloat) calculateOffLucky;
-(CGFloat) calculateOnLucky;

@end
