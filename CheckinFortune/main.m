//
//  main.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/22.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CiFAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CiFAppDelegate class]));
    }
}
