//
//  CiFAppDelegate.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/22.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CiFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)openSession;

@end
