//
//  CiFBackendUploadHelper.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CiFUser.h"
#import "CiFCheckin.h"

typedef void (^NullResponseHandler) (NSError *error);
typedef void (^UserResponseHandler) (CiFUser *user, NSError *error);
typedef void (^CheckinsResponseHandler) (NSArray *checkins, NSError *error);
typedef void (^CheckinIDCoordinateResponseHandler) (NSArray *idCoordPairs, NSError *error);

@interface CiFBackendUploadHelper : NSObject

+(void) addUserComplete: (NullResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) updateFriends:(NSArray *) userFriends complete: (NullResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) addCheckin:(CiFCheckin *) checkin complete: (NullResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) getUserByFBID:(NSString *) fbid complete: (UserResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) getCheckinsByIDs:(NSArray *) checkinIDs complete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) getCheckinsByFBIDs:(NSArray *) checkinFBIDs complete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) getUserCheckinsComplete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) getFriendCheckinsPaging:(NSInteger) pagingIndex complete: (CheckinsResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) getUserCheckinsCoordinateComplete: (CheckinIDCoordinateResponseHandler) completeHandler isAsynchronous:(BOOL) isa;

+(void) getFriendCheckinsCoordinatePaging:(NSInteger) pagingIndex complete: (CheckinIDCoordinateResponseHandler) completeHandler isAsynchronous:(BOOL) isa;


@end
