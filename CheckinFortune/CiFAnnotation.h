//
//  CiFAnnotation.h
//  CheckinFortune
//
//  Created by Li-YUan on 12/16/12.
//  Copyright (c) 2012 Teaualune Tseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CiFAnnotation : NSObject<MKAnnotation>{
}

-(id) initWithCoordinate:(CLLocationCoordinate2D) pCoordinate
                   Title:(NSString* )pTitle
             AndSubtitle:(NSString* )pSubtitle;

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;



@end
