//
//  FeedWallViewController.h
//  CheckinFortune
//
//  Created by En on 12/12/17.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface FeedWallViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate>
{
    //View
    IBOutlet MKMapView *mapView;
    IBOutlet UIView *viewWithBar;
    IBOutlet UITableView *tableCheck;
    
    //Btn
    IBOutlet UIButton *btnUpDown;
    IBOutlet UIButton *btnMySelf;
    IBOutlet UIButton *btnFriend;
    IBOutlet UIButton *btnNow;
    IBOutlet UIButton *btnRefresh;
    
    //data
    NSMutableArray *arrayOfMyCheck;
    NSMutableArray *arrayOfFriendCheck;
    NSMutableArray *arrayInPlist;
    //table Type
    int tableType;
}
@end
