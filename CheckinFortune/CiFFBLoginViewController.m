//
//  CiFFBLoginViewController.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/2.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFFBLoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CiFAppDelegate.h"

@interface CiFFBLoginViewController ()

@end

@implementation CiFFBLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    imageView = [[UIImageView alloc] init];
    [self.view addSubview:imageView];
    
    if ([[UIApplication sharedApplication].delegate window].bounds.size.height > 480) { // 568h
        imageView.bounds = CGRectMake(0, 0, 320, 568);
        imageView.image = [UIImage imageNamed:@"Default-568h.png"];
        
    } else {
        imageView.bounds = CGRectMake(0, 0, 320, 480);
        imageView.image = [UIImage imageNamed:@"Default.png"];
    }
    imageView.center = CGPointMake([[UIApplication sharedApplication].delegate window].center.x, [[UIApplication sharedApplication].delegate window].center.y - 20);
    //loginButton.hidden = YES;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.8 delay:0.5 options:UIViewAnimationCurveEaseOut animations:^{
        imageView.center = CGPointMake(imageView.center.x, imageView.center.y - imageView.bounds.size.height + 64);
    } completion:^(BOOL finished) {
        //loginButton.hidden = NO;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)login:(id)sender
{
    [((CiFAppDelegate *) [UIApplication sharedApplication].delegate) openSession];
    
//    [FBSession openActiveSessionWithReadPermissions:@[] allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//        if (!error) {
//            [[CiFMenuViewController sharedMenu] showAnotherContentViewControllerAtIndex:0 animated:NO];
//        }
//    }];
}

@end
