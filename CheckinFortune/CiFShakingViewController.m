//
//  CiFShakingViewController.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/15.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFShakingViewController.h"

#import "CiFCoreCheckinRecord.h"


@interface CiFShakingViewController ()
{
    NSInteger shakeTimes;
}

-(void) calculateLucky;
-(void) pushNextView;

@end

@implementation CiFShakingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    shakeTimes = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    shakeTimes ++;
    if (shakeTimes == 1) {
        
        [self calculateLucky];
        
        [UIView transitionWithView:boxImage duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            boxImage.image = [UIImage imageNamed:@"box1.png"];
        } completion:^(BOOL finished) {
            [self performSelector:@selector(pushNextView) withObject:nil afterDelay:0.5];
        }];
        
    }
}

-(void) calculateLucky
{
    [CiFCoreCheckinRecord currentCheckin].offLucky = [[CiFCoreCheckinRecord currentCheckin] calculateOffLucky];
    [CiFCoreCheckinRecord currentCheckin].onLucky = [[CiFCoreCheckinRecord currentCheckin] calculateOnLucky];
    
    [CiFCoreCheckinRecord currentCheckin].allLucky = [CiFCoreCheckinRecord currentCheckin].onLucky + [CiFCoreCheckinRecord currentCheckin].offLucky;
    
    [CiFCoreCheckinRecord currentCheckin].gold = 1000;
    [CiFCoreCheckinRecord currentCheckin].diamond = 3;
}

-(void) pushNextView
{
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CiFResultViewController"] animated:YES];
}

@end
