//
//  CiFCheckin.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#define STRING_C_I_D(CC, II, DD) CC.II = DD[@"II"]
#define VALUE_C_I_D(CC, II, DD) CC.II = (DD[@"II"]==nil)?[DD[@"II"] integerValue]:0

#import "CiFCheckin.h"
#import "GuaNain.h"

@interface CiFCheckin ()

// Lucky value calculation related

-(CGFloat) lightDarkFromGua:(NSInteger)gua nain:(NSInteger)nain;
-(CGFloat) compareElementFromGua:(NSInteger)gua nain:(NSInteger)nain;

-(int) guaIndexFromCoordinate;
-(int) nainIndexFromTime;

-(CGFloat) calculateLuckyByGuaIndex:(int)guaIndex nainIndex:(int)nainIndex;

@end

@implementation CiFCheckin

+(NSArray *) checkinsFromJSON:(id)JSON
{
    NSArray *a = (NSArray *) JSON;
    NSMutableArray *mCheckins = [[NSMutableArray alloc] initWithCapacity:[a count]];
    
    CiFCheckin *checkin = nil;
    for (NSDictionary *c in a) {
        checkin = [[CiFCheckin alloc] init];
        
        // properties might be nil
        STRING_C_I_D(checkin, checkinID, c);
        STRING_C_I_D(checkin, CheckinFBID, c);
        STRING_C_I_D(checkin, userFBID, c);
        STRING_C_I_D(checkin, placeFBID, c);
        STRING_C_I_D(checkin, message, c);
        STRING_C_I_D(checkin, time, c);
        checkin.coordinate = CLLocationCoordinate2DMake((c[@"latitude"]==nil)?[c[@"latitude"] doubleValue]:0.0, (c[@"longitude"]==nil)?[c[@"longitude"] doubleValue]:0.0);
        VALUE_C_I_D(checkin, offLucky, c);
        VALUE_C_I_D(checkin, onLucky, c);
        VALUE_C_I_D(checkin, allLucky, c);
        VALUE_C_I_D(checkin, gold, c);
        VALUE_C_I_D(checkin, diamond, c);
        
        [mCheckins addObject:checkin];
    }
    
    return [NSArray arrayWithArray:mCheckins];
}

+(NSArray *) checkinIDCoordinatesFromJSON:(id)JSON
{
    NSArray *a = (NSArray *) JSON;
    NSMutableArray *mCheckins = [[NSMutableArray alloc] initWithCapacity:[a count]];
    
    CiFCheckin *checkin = nil;
    for (NSDictionary *c in a) {
        checkin = [[CiFCheckin alloc] init];
        
        // properties might be nil
        STRING_C_I_D(checkin, checkinID, c);
        checkin.coordinate = CLLocationCoordinate2DMake((c[@"latitude"]==nil)?[c[@"latitude"] doubleValue]:0.0, (c[@"longitude"]==nil)?[c[@"longitude"] doubleValue]:0.0);
        [mCheckins addObject:checkin];
    }
    
    return [NSArray arrayWithArray:mCheckins];
}

#pragma mark - Lucky value calculation related

-(CGFloat) lightDarkFromGua:(int)gua nain:(int)nain
{
    if (gua == nain) return 1.0;
    else return 1.25;
}

-(CGFloat) compareElementFromGua:(int)gua nain:(int)nain
{
    NSInteger comp = (gua - nain) % 5;
    switch (comp) {
        case 0:
            return 1.0;
            break;
        case 1:
            return 0.6;
            break;
        case 2:
            return 0.75;
            break;
        case 3:
            return 1.2;
            break;
        case 4:
            return 0.85;
            break;
        default:
            return 0.0;
            break;
    }
}

-(int) guaIndexFromCoordinate
{
    int lat = (int) ((self.coordinate.latitude - (int) self.coordinate.latitude) * 100000);
    int lon = (int) ((self.coordinate.longitude - (int) self.coordinate.longitude) * 100000);
    
    return (lat + lon) % 64;
}

-(int) nainIndexFromTime
{
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    formatter.dateFormat = @"hh";
//    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
//    int hh = [[formatter stringFromDate:[NSDate date]] intValue];

    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970] - 3600.0;
    return ((int) (timeInterval/(3600*120))) % 60;
}

-(CGFloat) calculateOnLucky
{
    int i = [self guaIndexFromCoordinate];
    int j = [self nainIndexFromTime];
    
    return [self calculateLuckyByGuaIndex:i nainIndex:j] + 72;
}

-(CGFloat) calculateOffLucky
{
    int i = arc4random() % 64;
    int j = [self nainIndexFromTime];
    
    return [self calculateLuckyByGuaIndex:i nainIndex:j] + 72;
}

-(CGFloat) calculateLuckyByGuaIndex:(int)guaIndex nainIndex:(int)nainIndex
{    
    return gua_array[guaIndex][2]
    * nain_array[nainIndex][2]
    * [self compareElementFromGua:gua_array[guaIndex][0] nain:nain_array[nainIndex][0]]
    * [self lightDarkFromGua:gua_array[guaIndex][1] nain:nain_array[nainIndex][1]];
}

@end
