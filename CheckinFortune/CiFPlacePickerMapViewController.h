//
//  CiFPlacePickerMapViewController.h
//  CheckinFortune
//
//  Created by Li-YUan on 12/16/12.
//  Copyright (c) 2012 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>
#import "CiFAnnotation.h"

@interface CiFPlacePickerMapViewController : UIViewController <CLLocationManagerDelegate, FBPlacePickerDelegate, MKMapViewDelegate, UITableViewDataSource ,  UITableViewDelegate> {
    IBOutlet MKMapView *placeMapView;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) CiFAnnotation *chosenAnnotation;


@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocationCoordinate2D locationCoordinate;
@property (nonatomic) NSArray *placeResult;

- (IBAction)cancel:(id)sender;


@end
