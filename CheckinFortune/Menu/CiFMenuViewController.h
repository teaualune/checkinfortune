//
//  CiFMenuViewController.h
//  FBMenuDemo
//
//  Created by Teaualune Tseng on 12/11/20.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//
//  Usage:
//  1. Add five files to project
//  2. Include CiFMenuViewController.h file at app delegate and run setupAtApplicationDidFinishLaunching at applicationDidFinishLaunching: withOption:
//  3. In any views that require to show menu, call [[CiFMenuViewController sharedMenu] showMenu]
//  4. Go to CiFMenuDataSourceHelper.m to edit content views.

#import <UIKit/UIKit.h>

#define MENU_WIDTH 265

@interface CiFMenuViewController : UIViewController<UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *menuTableView;
}

@property (nonatomic, strong) NSDictionary *contentViewControllers;
@property (nonatomic, strong) UIView *screenShotView;
@property (nonatomic, weak) UIViewController *currentViewController;

+(void)setupAtApplicationDidFinishLaunching;
+(CiFMenuViewController *) sharedMenu;
- (void)showAnotherContentViewControllerAtIndex:(NSInteger) index animated:(BOOL) animate;
-(void) showMenu;
-(void) showMenu:(id)sender;

@end
