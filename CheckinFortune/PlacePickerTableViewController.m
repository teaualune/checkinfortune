//
//  PlacePickerTableViewController.m
//  CheckinFortune
//
//  Created by Li-YUan on 12/15/12.
//  Copyright (c) 2012 Teaualune Tseng. All rights reserved.
//

#import "PlacePickerTableViewController.h"

#import "CiFCoreCheckinRecord.h"

#define RESULTSLIMIT 30
#define RADIUSINMETERS 500

@interface PlacePickerTableViewController ()
{
    NSInteger previousIndexPathRow;
    NSInteger currentIndexPathRow;
}

@end

@implementation PlacePickerTableViewController

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.placeResult == nil) return 0;
    else return [self.placeResult count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    if (indexPath.row == currentIndexPathRow) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (self.placeResult[indexPath.row] != nil) {
        cell.textLabel.text = self.placeResult[indexPath.row][@"name"];
    }

    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.activityIndicator startAnimating];
    [self locationManager];
    
    self.placeResult = nil;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    previousIndexPathRow = currentIndexPathRow = -1;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.locationManager startUpdatingLocation];
    self.nextButton.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    if (newLocation.horizontalAccuracy < 100) {
        // We wait for a precision of 100m and turn the GPS off
        [self.locationManager stopUpdatingLocation];
        self.locationManager = nil;
        
        self.locationCoordinate = newLocation.coordinate;
        
        NSLog(@"%f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        
        [FBRequestConnection startForPlacesSearchAtCoordinate:self.locationCoordinate radiusInMeters:RADIUSINMETERS resultsLimit:RESULTSLIMIT searchText:nil completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            //NSLog(@"%@", result);
            for(NSDictionary *dic in result[@"data"]){
                NSLog(@"%@", dic);
            }
            self.placeResult = [NSArray arrayWithArray:result[@"data"]];
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
        }];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    previousIndexPathRow = currentIndexPathRow;
    currentIndexPathRow = indexPath.row;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    if (previousIndexPathRow != -1) {
        NSIndexPath *previousIndexPath = [NSIndexPath indexPathForRow:previousIndexPathRow inSection:0];
        [tableView cellForRowAtIndexPath:previousIndexPath].accessoryType = UITableViewCellAccessoryNone;
    }
    
    [CiFCoreCheckinRecord currentCheckin].placeFBID = self.placeResult[indexPath.row][@"id"];
    
    self.nextButton.enabled = YES;
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
