//
//  CiFAnnotation.m
//  CheckinFortune
//
//  Created by Li-YUan on 12/16/12.
//  Copyright (c) 2012 Teaualune Tseng. All rights reserved.
//

#import "CiFAnnotation.h"

@implementation CiFAnnotation

-(id) initWithCoordinate:(CLLocationCoordinate2D) pCoordinate
                   Title:(NSString* )pTitle
             AndSubtitle:(NSString* )pSubtitle{
    self = [super init];
    if(self){
        _coordinate = pCoordinate;
        _title = pTitle;
        _subtitle = pSubtitle;
    }
    return self;
}



@end
