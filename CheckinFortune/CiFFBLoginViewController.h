//
//  CiFFBLoginViewController.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/2.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>

@interface CiFFBLoginViewController : FBViewController
{
    UIImageView *imageView;
    IBOutlet UIButton *loginButton;
}

-(IBAction)login:(id)sender;

@end
