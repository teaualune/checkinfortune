//
//  CiFPostResultViewController.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/15.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <MapKit/MapKit.h>

@interface CiFPostResultViewController : UIViewController<FBFriendPickerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    NSMutableSet *tags;
    IBOutlet MKMapView *backgroundMapView;
    IBOutlet UIImageView *uploadPhotoView;
    
    IBOutlet UIView *containerView;
    
    UIImagePickerController* pickerController;
}

@property (weak, nonatomic) IBOutlet UITextView *messageTextView;

@property (weak, nonatomic) IBOutlet UILabel *luckyLabel;

-(IBAction)post:(id)sender;
-(IBAction)tag:(id)sender;
-(IBAction)photo:(id)sender;

@end
