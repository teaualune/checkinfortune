//
//  CiFResultViewController.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/15.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CiFResultViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *leftRsltAnimal;
@property (weak, nonatomic) IBOutlet UIImageView *midRsltAnimal;
@property (weak, nonatomic) IBOutlet UIImageView *rightRsltAnimal;
//@property (weak, nonatomic) IBOutlet UIImageView *leftRsltCloud;
@property (weak, nonatomic) IBOutlet UIImageView *midRsltCloud;
@property (weak, nonatomic) IBOutlet UIImageView *rightRsltCloud;
@property (weak, nonatomic) IBOutlet UIImageView *decimalNum;
@property (weak, nonatomic) IBOutlet UIImageView *singleNum;
@property (weak, nonatomic) IBOutletCollection(UIImageView) NSArray *leftRsltCloud;

@property (weak, nonatomic) IBOutlet UILabel *allLuckyLabel;
@property (weak, nonatomic) IBOutlet UILabel *onLuckyLabel;
@property (weak, nonatomic) IBOutlet UILabel *offLuckyLabel;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;
@property (weak, nonatomic) IBOutlet UILabel *diamondLabel;
@property (weak, nonatomic) IBOutlet UIImageView *goldImage;
@property (weak, nonatomic) IBOutlet UIImageView *diamondImage;

@property (weak, nonatomic) IBOutlet UILabel *luckyLabel;

-(IBAction)goToPostView:(id)sender;

@end
