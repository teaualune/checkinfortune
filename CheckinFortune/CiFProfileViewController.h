//
//  CiFProfileViewController.h
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/12/19.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CiFProfileViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UIView *profileView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *goldImage;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;

@property (weak, nonatomic) IBOutlet UIImageView *diamondImage;
@property (weak, nonatomic) IBOutlet UILabel *diamondLabel;

@property (weak, nonatomic) IBOutlet UIImageView *luckyImage;

@property (weak, nonatomic) IBOutlet UILabel *luckyLabel;

@property (weak, nonatomic) IBOutlet UIImageView *settingImage;

-(IBAction)showMenu:(id)sender;

@end
