//
//  CiFFacebookUploadHelper.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/29.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFFacebookUploadHelper.h"
#import "CiFUser.h"

@implementation CiFFacebookUploadHelper

-(void) getFriendsCheckins:(FBRequestHandler)completion
{
    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        NSLog(@"%@", result);
        completion(connection, result, error);
    }];
}

@end
