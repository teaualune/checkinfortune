//
//  CiFAppDelegate.m
//  CheckinFortune
//
//  Created by Teaualune Tseng on 12/11/22.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import "CiFAppDelegate.h"
#import "CiFMenuViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CiFFacebookUploadHelper.h"

#import "CiFCheckin.h"
#import "CiFUser.h"

@interface CiFAppDelegate ()

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;

@end

@implementation CiFAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [CiFMenuViewController setupAtApplicationDidFinishLaunching];
    if (![FBSession activeSession].isOpen) {
        UIViewController *vc = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"FBLoginViewController"];
        self.window.rootViewController = vc;
    }
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

#pragma mark - Facebook related

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
            [[CiFMenuViewController sharedMenu] showAnotherContentViewControllerAtIndex:0 animated:NO];
            
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                CiFUser *loginUser = [CiFUser currentUser];
                if (error) {
                    loginUser.userFBID = loginUser.userName = @"";
                }
                else {
                    loginUser.userFBID = result[@"id"];
                    loginUser.userName = result[@"name"];
                    
                    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                        if (error) return;
                        NSArray *friends = ((NSDictionary *) result)[@"data"];
                        
                        __block int i = 0;
                        for (NSDictionary *user in friends) {
                            [FBRequestConnection startWithGraphPath:[user[@"id"] stringByAppendingString:@"/checkins"] completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                if (error) return;
                                i++;
                                for (NSDictionary *d in result[@"data"]) {
                                    [[CiFUser friendsCheckins] addObject:d];
                                }
                                if (i == [friends count]) {
                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"CiFGetFriendsChekinsNotification" object:self];
                                    NSLog(@"get friends checkin\n%@", [CiFUser friendsCheckins]);
                                }
                            }];
                        }
                        [FBRequestConnection startWithGraphPath:@"me/checkins" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                            if (error) return;
                            [[CiFUser friendsCheckins] addObject:result[@"data"]];
                        }];
                     
                    }];
                    
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CiFFacebookUserloggedinNotification" object:self];
            }];
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)openSession
{
    [FBSession openActiveSessionWithReadPermissions:@[@"user_status", @"user_location"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
     }];
}

@end
